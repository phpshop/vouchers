<?php 
if(isset($errorMsg) && $errorMsg != false && $success == false)
{
  ?>
    <div class="alert alert-danger text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
  <?php
}
else if(isset($errorMsg) && $errorMsg != false && $success == true)
{
?>
    <div class="alert alert-info text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
<?php
}
?>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5">Purchase</h1>
              <form name="purchaseform" action="<?php echo $config->baseUrl; ?>controller.php" method="POST" id="purchaseform">
              <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Name</label>
                          <input type="text" class="form-control required" id="name" name="name" value="testing" placeholder="Name">
                        </div>
                      <div class="form-group col-md-6">
                          <label for="email">Email</label>
                          <input type="email" class="form-control required email" id="emailValue" value="test@systraq.com" name="emailValue" placeholder="Email">
                      </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAddress">Voucher Number</label>
                    <input type="text" class="form-control" name="vouchernumber" id="vouchernumber" value="2170000000015" placeholder="Voucher Number">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputAddress2">Pin</label>
                  <input type="text" class="form-control" id="pin" name="pin" placeholder="Pin" value="test@systraq.com">
                </div>
              </div>

              
              <div class="form-row">
                  <div class="form-group col-md-6">
                  <label for="inputCity">Mobile Number</label>
                  <input type="number" class="form-control" id="inputCity" name="mobile" id="mobile" placeholder="Mobile">
                  </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="inputState">Address </label>
                    <?php $requiredClass = ($config->physicalVoucher == true) ? 'required' : ''; ?>
                    <textarea class="form-control <?php   echo $requiredClass; ?>" name="address" id="address" placeholder="Address"></textarea>
                </div>
              </div>


              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputState">Value</label>
                <?php if($config->demonominations == 'open') {
                  ?>
                      <input type="text" name="amount_value" id="amount_value" class="form-control"/>
                 <?php
                }
                else if(is_array($config->demonominations))
                {
                ?>
                <select class="form-control" name="amount_value" id="amount_value">
                    <?php foreach ($config->demonominations as $demomination) {
                      ?>
                        <option value="<?php echo $demomination; ?>"><?php echo $demomination; ?></option>
                      <?php
                      # code...
                    } ?>
                </select>
                  <?php
                }
                else
                {
                ?>

                  <?php
                }

                ?>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <div class="form-check">
                  <input class="form-check-input" name="giftcheck" type="checkbox" id="giftcheck" value="1" name="giftcheck">
                    <label class="form-check-label" for="giftcheck">
                      Gift
                    </label>
                </div>
              </div> 

              <div id="secondaryEmailArea" name="secondaryEmailArea" class="form-group d-none col-md-12">
                    <label>Secondary Email</label>
                    <input type="email" name="secondary_email" id="secondary_email" class="form-control">
              </div>
            </div>


            <div class="form-row">
              <div class="form-group col-md-12">
              <label for="message">Message</label>
                  <textarea class="form-control" name="message" maxlength="1000"  id="message" placeholder="Message"></textarea>
              </div>
            </div>

             <div class="form-row">
              <input type="hidden" name="action" id="action" value="processpayment" />
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>



              </form>
        </div>
      </div>
    </div>