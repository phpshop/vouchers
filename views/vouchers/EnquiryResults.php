<?php 
if(isset($errorMsg) && $errorMsg != false && $success == false)
{
  ?>
    <div class="alert alert-danger text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
  <?php
}
else if(isset($errorMsg) && $errorMsg != false && $success == true)
{
?>
    <div class="alert alert-info text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
<?php
}
?>

<main role="main" class="container">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="border-bottom border-gray pb-2 mb-0">Recent updates</h6>

      <?php if(isset($voucherObj) && $voucherObj != FALSE)
      {
        ?>
      
        <div class="form-row">

        <?php
        foreach ($voucherObj as $key => $value) {
          if($key != 'transactions')
          {
            ?>
          <div class="form-group col-md-12">
      
          <div class="media text-muted pt-12">

              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark"><?php echo ucfirst($key) ?></strong>
                <?php echo $value; ?>
          </p>
        </div>


 

            
          </div>
          <?php
          } 
        }
      }
      ?>


      <div class="accordion" id="accordionExample">
          <div class="card">
          <div class="card-header" id="headingOne">
          <h5 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Transaction Data
          </button>
          </h5>
          </div>

          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
 

          <?php   
  
          $counter = 0;
          foreach ($voucherObj->transactions as $key => $value) {
          ?>

          <?php
        
          foreach($value as $key =>$transaction)
          {
            ?>
              <p><strong><?php echo ucfirst($key) ?></strong>: <?php echo $transaction; ?></p>
            <?php
        
          }

         ?>
         <hr>
          
         <?php
            $counter++;
          }


         ?> 



          </div>
          </div>
          </div>
      </div>



      </div>

    </main>