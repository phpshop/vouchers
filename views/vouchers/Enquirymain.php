<?php 
if(isset($errorMsg) && $errorMsg != false && $success == false)
{
  ?>
    <div class="alert alert-danger text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
  <?php
}
else if(isset($errorMsg) && $errorMsg != false && $success == true)
{
?>
    <div class="alert alert-info text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
<?php
}



?>


    <!-- Page Content -->
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
			<h1 class="mt-5">Enquiry</h1>
			<form name="purchaseform" action="<?php echo $config->baseUrl; ?>controller.php" method="POST" id="enquiryform">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputEmail4">Voucher Number</label>
					<input type="text" class="form-control required" id="vouchernumber" name="vouchernumber" value="" placeholder="Voucher Number">
				</div>
				<div class="form-group col-md-6">
					<label for="pin">Pin</label>
					<input type="text" class="form-control required email" id="pin" value="test@systraq.com" name="pin" placeholder="PIN">
				</div> 
			</div>



	
             <div class="form-row">
              <input type="hidden" name="action" id="action" value="processEnquiry" />
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>

			</form>
			</div>
			</div>
		</div>




