<?php 
if(isset($errorMsg) && $errorMsg != false && $success == false)
{
  ?>
    <div class="alert alert-danger text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
  <?php
}
else if(isset($errorMsg) && $errorMsg != false && $success == true)
{
?>
    <div class="alert alert-info text-center" role="alert">
      <strong><?php echo $errorMsg; ?></strong>
    </div>
<?php
}
?>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5"></h1>
           


                <form name="checkoutform" action="<?php echo $config->baseUrl; ?>controller.php?action=redirectfromstripe&hashcrsf=<?php echo rand(); ?>" method="POST">
<input type="hidden" name="stripeToken" id="stripe_token" />
</form>

<script src="https://checkout.stripe.com/checkout.js"></script>


<button id="customButton" style="display:none;">Purchase</button>
<script>
    var handler = StripeCheckout.configure({
    key: "pk_test_aZnQYilnsd6OkTtx99Pjaqj9",
    token: function(token, args) {
        document.getElementById("stripe_token").value = token.id;
        document.checkoutform.submit();
    }});

    var click_event = document.getElementById('customButton').addEventListener('click', function(e) {

    // Open Checkout with further options
    handler.open({
        name: "<?php if(isset($config->sitename)){ echo $config->sitename ; } ?>",
        description: "Voucher charge",
        amount: "<?php if(isset($amount)) { echo $amount; } ?>", // If cost is 2 USD, then value here should be 2*100=200
        currency: "aud",
        email: "USER_EMAIL",
        image: "https://stripe.com/img/documentation/checkout/marketplace.png"
    });
    e.preventDefault();

    });

    $(window).load(function(){
    $("button#customButton").trigger('click');
    });

</script>









                
        </div>
      </div>
    </div>





