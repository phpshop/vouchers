 <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5">Error</h1>
          <p class="lead">System is not configured to allow purchasing of vouchers!</p>

        </div>
      </div>
    </div>
