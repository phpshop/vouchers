<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Voucher System</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $config->baseUrl; ?>/assests/custom.css" rel="stylesheet">  
    <?php if(!empty($config->css)) { ?>
          <link href="<?php echo $config->css ; ?>" rel="stylesheet">  
    <?php } ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
      <div class="container">
           <?php if(isset($config->companylogo) && !empty($config->companylogo)) { ?>
            <a href="<?php echo $config->baseUrl; ?>"><img src="https://via.placeholder.com/100x100"></a>
        <?php }
        else
         {?>
                    <a class="navbar-brand" href="/">Voucher System</a>
         <?php  } ?>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="<?php echo $config->baseUrl; ?>">Purchase
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $config->baseUrl; ?>?action=EnquiryVoucher">Enquiry</a>
              </li>
            </ul>
          </div>
      </div>
    </nav>

   
