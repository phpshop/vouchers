<?php
/// include the paypal urls
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

ini_set('allow_url_include', true);
session_start();


// if(!@include("includes/WebVoucher.php.inc")) throw new Exception("System is not configured to allow purchasing of vouchers");
require 'vendor/autoload.php';
if (file_exists('includes/WebVoucher.php.inc')) {
    include 'includes/WebVoucher.php.inc';
    $action  = '';
    $company = '';
    $key     = '';

    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    }
    if (isset($_GET['company']) && isset($_GET['key'])) {
        $company = $_GET['company'];
        $key     = $_GET['key'];
        ### this is the get method
    }

    $configParameters = getOurParameters($company, $key);
    $jsonencoded      = json_encode($configParameters);

    if (empty($configParameters)) {
        MissingConfig();
        return;
    }

    //// assign the session into a variable

    $_SESSION['configsession'] = $jsonencoded;

    $name      = (isset($_POST['name'])) ? $_POST['name'] : '';
    $email     = (isset($_POST['email'])) ? $_POST['email'] : '';
    $giftEmail = (isset($_POST['secondary_email'])) ? $_POST['secondary_email'] : '';

    $voucher = (isset($_POST['vouchernumber'])) ? $_POST['vouchernumber'] : '';
    $pin     = (isset($_POST['pin'])) ? $_POST['pin'] : '';
    $mobile  = (isset($_POST['mobile'])) ? $_POST['mobile'] : '';
    $amount  = (isset($_POST['amount_value'])) ? $_POST['amount_value'] : '';
    $address = (isset($_POST['address'])) ? $_POST['address'] : '';

    $giftAddress = (!empty($giftEmail)) ? $address : '';

    $gift    = (isset($_POST['giftcheck'])) ? $_POST['giftcheck'] : '';
    $message = (isset($_POST['message'])) ? $_POST['message'] : '';

    ///
    $config = handleConfig();
    switch ($action) {
        case 'processpayment':
            if ($config->paymentGateway == 'Eway') {
                $ewayHostedUrl = GenerateSharedEwayUrl($amount);
            }
            if ($config->paymentGateway == 'stripe') {
                $stripeTrans = StripePaymentPage();

            }
            if ($config->paymentGateway == 'paypal') {
                $paypalTrans = processPayPal();
            }
            break;
        case 'returnfrompaypal':
            returnFromPaypal();
            break;
        case 'paypalcancel':
            paypalcancel();
            break;
        case 'EnquiryVoucher':
            IntialiseEnquiryPage();
            break;
        case 'processEnquiry':
            processEnquiry();
            break;
        case 'redirectfromeway':
            redirectfromeway();
            break;
        case 'redirectfromstripe':
            redirectfromstripe();
            break;
        default:
            IntialiseVoucherPage();
            break;
    }

    /// render the main screen to process the vouchers

    /// now do the config call
} else {
    MissingConfig();
    return;
}

function MissingParameters()
{
    include '/views/header.php';

    include '/views/footer.php';
}

function EnquiryView()
{
    include 'views/header.php';
    include 'views/vouchers/Enquirymain.php';
    include 'views/footer.php';
}

function StripePaymentPage()
{
    $config = handleConfig();
// put all the input data to a session
    buildInputSession();
    $amount = $_SESSION['inputDataArray']['amount'];
    $amount = $amount * 100;
    $amount = calculateTotal($amount);

    include 'views/header.php';
    include 'views/stripepayments/main.php';
    include 'views/footer.php';
}

function processEnquiry()
{

    $vouchernumber  = (isset($_POST['vouchernumber'])) ? $_POST['vouchernumber'] : '';
    $pin            = (isset($_POST['pin'])) ? $_POST['pin'] : '';
    $EnquiryVoucher = EnquiryRegardingVoucher($vouchernumber, $pin);

    if ($EnquiryVoucher['status'] == 'Error') {
        ### this is the error message
        $errorMsg = "Voucher Error - " . $EnquiryVoucher['msg'];
        IntialiseEnquiryPage($errorMsg);

    } else if ($EnquiryVoucher['status'] == 'Valid') {
        $enquireVoucher = $EnquiryVoucher['returnobj']->voucher;
        IntialiseEnquiryResultsPage(false, false, $enquireVoucher);
    }

}

function IntialiseVoucherPage($errorMsg = false, $success = false, $voucherObj = false)
{
    $config = handleConfig();
    include 'views/header.php';

    if (empty($config->html)) {
        include 'views/vouchers/main.php';
    } else {
        $iframeUrl    = $config->html;
        $htmltorender = file_get_contents($config->html);
        include 'views/vouchers/remotehtml.php';
    }

    include 'views/footer.php';
}

function IntialiseEnquiryPage($errorMsg = false, $success = false, $voucherObj = false)
{
    $config = handleConfig();
    include 'views/header.php';

    if (empty($config->html)) {
        include 'views/vouchers/Enquirymain.php';
    } else {
        $iframeUrl    = $config->html;
        $htmltorender = file_get_contents($config->html);
        include 'views/vouchers/remotehtml.php';
        //include $config->html;
    }

    include 'views/footer.php';
}

function IntialiseEnquiryResultsPage($errorMsg = false, $success = false, $voucherObj = false)
{
    $config = handleConfig();
    include 'views/header.php';

    if (empty($config->html)) {
        include 'views/vouchers/EnquiryResults.php';
    } else {
        $iframeUrl    = $config->html;
        $htmltorender = file_get_contents($config->html);
        include 'views/vouchers/remotehtml.php';
        //include $config->html;
    }

    include 'views/footer.php';
}

function MissingConfig()
{
    $config = handleConfig();
    include 'views/header.php';
    include 'views/errors/missingconfig.php';
    include 'views/footer.php';
}

function GenerateSharedEwayUrl($amount)
{
    $config               = handleConfig();
    $paymentGatewayConfig = $config->paymentGatewayParameters;
    ###
    $apiKey           = $paymentGatewayConfig->apikey;
    $apiPassword      = $paymentGatewayConfig->apipassword;
    $cardDetailsArray = $paymentGatewayConfig->CardDetails;

    $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
    $client      = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

    $redirectUrl = $config->paymentGatewayParameters->RedirectUrl;
    $cancelUrl   = $config->paymentGatewayParameters->CancelUrl;    


     $calculateTotal = calculateTotal($amount); 
     $total = $calculateTotal * 100;


    $calculatedAmount = number_format((float)$amount, 2, '.', '');



    $transaction = [
        'RedirectUrl'     => $redirectUrl,
        'CancelUrl'       => 'http://www.eway.com.au',
        'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        'Payment'         => [
            'TotalAmount' => calculateTotal($total),
        ],
        'Customer'        => [
            'Reference'      => rand() . "Voucher",
            'Title'          => '',
            'FirstName'      => '',
            'LastName'       => '',
            'CompanyName'    => '',
            'JobDescription' => '',
            'Street1'        => '',
            'Street2'        => '',
            'City'           => '',
            'State'          => '',
            'PostalCode'     => '',
            'Country'        => '',
            'Phone'          => '',
            'Mobile'         => '',
            'Email'          => '',
            "Url"            => "",
        ],
    ];

    $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::RESPONSIVE_SHARED, $transaction);

    if (!$response->getErrors()) {

        // put all the input data to a session
        buildInputSession();

        // Redirect to the Responsive Shared Page
        header('Location: ' . $response->SharedPaymentUrl);
        die();
    } else {
        foreach ($response->getErrors() as $error) {
            echo "Error: " . \Eway\Rapid::getMessage($error) . "<br>";
        }
    }

}





####
function buildInputSession()
{
    $name      = (isset($_POST['name'])) ? $_POST['name'] : '';
    $email     = (isset($_POST['email'])) ? $_POST['email'] : '';
    $giftEmail = (isset($_POST['secondary_email'])) ? $_POST['secondary_email'] : '';

    $voucher = (isset($_POST['vouchernumber'])) ? $_POST['vouchernumber'] : '';
    $pin     = (isset($_POST['pin'])) ? $_POST['pin'] : '';
    $mobile  = (isset($_POST['mobile'])) ? $_POST['mobile'] : '';
    $amount  = (isset($_POST['amount_value'])) ? $_POST['amount_value'] : '';
    $address = (isset($_POST['address'])) ? $_POST['address'] : '';

    $giftAddress = (!empty($giftEmail)) ? $address : '';

    $gift    = (isset($_POST['giftcheck'])) ? $_POST['giftcheck'] : '';
    $message = (isset($_POST['message'])) ? $_POST['message'] : '';

    $inputDataArray = array('name' => $name,
        'email'                        => $email,
        'giftemail'                    => $giftEmail,
        'voucher'                      => $voucher,
        'pin'                          => $pin,
        'mobile'                       => $mobile,
        'amount'                       => $amount,
        'address'                      => $address,
        'giftaddress'                  => $giftAddress,
        'gift'                         => $gift,
        'message'                      => $message,
    );

    $_SESSION['inputDataArray'] = $inputDataArray;

}

function processPayPal()
{

    $config = handleConfig();
    ### handle the session
    buildInputSession();
    // Autoload SDK package for composer based installations
    $amounttoCharge = calculateTotal($_POST['amount_value']);

    $voucher = (isset($_POST['vouchernumber'])) ? $_POST['vouchernumber'] : '';

    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            $config->paymentGatewayParameters->clientid,
            $config->paymentGatewayParameters->secret
        )
    );

    // Create new payer and method
    $payer = new Payer();
    $payer->setPaymentMethod("paypal");
    // Set redirect URLs
    $redirectUrls = new RedirectUrls();

    $redirectUrls->setReturnUrl($config->paymentGatewayParameters->returnUrl . "&voucherNum=" . $voucher)
        ->setCancelUrl($config->paymentGatewayParameters->cancelUrl);

    // Set payment amount
    $amount = new Amount();
    $amount->setCurrency($config->paymentGatewayParameters->currecy)
        ->setTotal($amounttoCharge);

    // Set transaction object
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setDescription("Payment description");

    // Create the full payment object
    $payment = new Payment();
    $payment->setIntent('sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    // Create payment with valid API context
    try {
        $payment->create($apiContext);
        // Get PayPal redirect URL and redirect the customer
        $approvalUrl = $payment->getApprovalLink();
        header('Location: ' . $approvalUrl);
        // Redirect the customer to $approvalUrl
    } catch (PayPal\Exception\PayPalConnectionException $ex) {
        echo $ex->getCode();
        echo $ex->getData();
        die($ex);
    } catch (Exception $ex) {
        die($ex);
    }

}

function handleConfig()
{
    $configObj = json_decode($_SESSION['configsession']);
    return $configObj;
}

function paypalcancel()
{
    $errorMsg = "Failed to process the paypal payment";
    IntialiseVoucherPage($errorMsg);
}

function returnFromPaypal()
{

    $inputDataArray = (isset($_SESSION['inputDataArray'])) ? $_SESSION['inputDataArray'] : '';
    $config         = handleConfig();

    $voucherArray = array(
        'serialnumber' => $inputDataArray['voucher'],
        'site'         => $config->site,
        'pin'          => $inputDataArray['pin'],
        'amount'       => $inputDataArray['amount'],
        'customername' => $inputDataArray['name'],
        'address'      => $inputDataArray['address'],
        'phone'        => $inputDataArray['mobile'],
        'email'        => $inputDataArray['email'],
        'isgift'       => $inputDataArray['gift'],
        'giftemail'    => $inputDataArray['gift'],
        'giftaddress'  => $inputDataArray['giftaddress'],
        'reference'    => '',
        'message'      => $inputDataArray['message'],
    );

    if (isset($_REQUEST['paymentId'])) {
        #### then we process the appliction
        if ($_REQUEST['voucherNum'] == '') {
            #### now go and create the voucher via the api
            $createVoucherobj = CreateVoucher($voucherArray);
            #### create the voucher
            CreateVoucherProcess($createVoucherobj);
        } else {
            #### do a inquiry to check

            $makeInquiry = EnquiryRegardingVoucher($inputDataArray['voucher'], $inputDataArray['pin']);

            if ($makeInquiry['status'] == 'Error') {
                ### this is the error message
                $errorMsg = "Voucher Error - " . $makeInquiry['msg'];
                IntialiseVoucherPage($errorMsg);

            } else if ($makeInquiry['status'] == 'Valid') {
                $createVoucherobj = RechargeVoucher($voucherArray);
                ###t this is the voucher
                if ($createVoucherobj->result == '0') {
                    $successMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $successMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($successMsg, true);
                } else {
                    $ErrorMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $ErrorMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($ErrorMsg, true);
                }
                #### process the recharge.

            }

        }

        // $successMsg = "Payment Processed - Transaction ID ".$_REQUEST['paymentId'];
        // SuccessPage($successMsg);
    } else {
        $errorMsg = "Failed to process the paypal payment";
        IntialiseVoucherPage($errorMsg);
    }

}

function SuccessPage($sucessMsg = false)
{
    include 'views/header.php';
    $config = handleConfig();
    include 'views/vouchers/sucess.php';
    include 'views/footer.php';
}

function EnquiryRegardingVoucher($vouchernumber, $pin)
{
    $host = 'https://sandbox.zenglobal.net/v1/voucher/Gift/enquire';
    //// do the enquiry
    $username = '1D25B193-3D67-4637-A7F6-8ABE1DA234';
    $password = '12345678904343';

    $data['voucher'] = array('serialnumber' => $vouchernumber, #2170000000015
    'pin'                                   => $pin,
    );

    // jSON String for request
    $json_string = json_encode($data);

    // Initializing curl
    $ch = curl_init($host);

    // Configuring curl options
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_USERPWD        => $username . ":" . $password, // authentication
        CURLOPT_HTTPHEADER     => array('Content-type: application/json'),
        CURLOPT_POSTFIELDS     => $json_string,
    );

    // Setting curl options
    curl_setopt_array($ch, $options);
    // Getting results
    $result    = curl_exec($ch); // Getting jSON result string
    $resultobj = json_decode($result);
    $response  = array();
    if (isset($resultobj->errormessage)) {
        $response['status'] = 'Error';
        $response['msg']    = $resultobj->errormessage;
    } else {
        $response['status']    = 'Valid';
        $response['returnobj'] = $resultobj;
    }
    return $response;
}

function CreateVoucher($voucherArray)
{

    $host = 'https://sandbox.zenglobal.net/v1/voucher/Gift/create';
    //// do the enquiry
    $username = '1D25B193-3D67-4637-A7F6-8ABE1DA234';
    $password = '12345678904343';

    $data['voucher'] = $voucherArray;
    // jSON String for request
    $json_string = json_encode($data);

    // Initializing curl
    $ch = curl_init($host);

    // Configuring curl options
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_USERPWD        => $username . ":" . $password, // authentication
        CURLOPT_HTTPHEADER     => array('Content-type: application/json'),
        CURLOPT_POSTFIELDS     => $json_string,
    );

    // Setting curl options
    curl_setopt_array($ch, $options);
    // Getting results
    $result    = curl_exec($ch); // Getting jSON result string
    $resultobj = json_decode($result);

    return $resultobj;

}

function RechargeVoucher($voucherArray)
{

    $host = 'https://sandbox.zenglobal.net/v1/voucher/Gift/recharge';
    //// do the enquiry
    $username = '1D25B193-3D67-4637-A7F6-8ABE1DA234';
    $password = '12345678904343';

    $data['voucher'] = $voucherArray;
    // jSON String for request
    $json_string = json_encode($data);

    // Initializing curl
    $ch = curl_init($host);

    // Configuring curl options
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_USERPWD        => $username . ":" . $password, // authentication
        CURLOPT_HTTPHEADER     => array('Content-type: application/json'),
        CURLOPT_POSTFIELDS     => $json_string,
    );

    // Setting curl options
    curl_setopt_array($ch, $options);
    // Getting results
    $result    = curl_exec($ch); // Getting jSON result string
    $resultobj = json_decode($result);

    return $resultobj;

}

function CreateVoucherProcess($creatVoucherObj)
{
    $config = handleConfig();
    $msg    = '';

    if ($config->physicalVoucher == true) {
        $msg .= '<p>The voucher would be emailed to you.</p>';
    }

    if ($creatVoucherObj->result == 9 || !empty($creatVoucherObj->reason)) {
        $msg .= $creatVoucherObj->errormessage . "</br>" . $creatVoucherObj->reason;
        //// pass the the main screen
        IntialiseVoucherPage($msg);
    } else {
        $msg = "<p><h2>Voucher Created</h2></p><p>" . $creatVoucherObj->information . "</p><p>Serial Number:" . $creatVoucherObj->serialnumber . "</p>";
        IntialiseVoucherPage($msg, true);

    }

}

function calculateTotal($amount)
{
    $config     = handleConfig();
    $percentage = (!empty($config->{'surcharge%'})) ? $config->{'surcharge%'} / 100 : '';
    //The percent that we want to add on to our number.
    $percentageChange = $config->{'surcharge%'};
    $SurchargeAmount  = $config->{'surcharge$'};
    $newNumber        = 0;

    if (!empty($percentageChange)) {
        //Our original number.
        $originalNumber = round($amount, 2);

        //Get 2.25% of 100.
        $numberToAdd = ($originalNumber / 100) * $percentageChange;

        //Finish it up with some simple addition
        $newNumber = $originalNumber + $numberToAdd;

    }

    if (!empty($SurchargeAmount)) {

        $SurchargeAmountIncrementNumber = $amount + $SurchargeAmount;
        $newNumber                      = $newNumber + $SurchargeAmountIncrementNumber;
    }

    return $newNumber;

}

function redirectfromeway()
{

    $inputDataArray = (isset($_SESSION['inputDataArray'])) ? $_SESSION['inputDataArray'] : '';
    $config         = handleConfig();

    $voucherArray = array(
        'serialnumber' => $inputDataArray['voucher'],
        'site'         => $config->site,
        'pin'          => $inputDataArray['pin'],
        'amount'       => $inputDataArray['amount'],
        'customername' => $inputDataArray['name'],
        'address'      => $inputDataArray['address'],
        'phone'        => $inputDataArray['mobile'],
        'email'        => $inputDataArray['email'],
        'isgift'       => $inputDataArray['gift'],
        'giftemail'    => $inputDataArray['gift'],
        'giftaddress'  => $inputDataArray['giftaddress'],
        'reference'    => '',
        'message'      => $inputDataArray['message'],
    );

    if (isset($_REQUEST['AccessCode'])) {
        #### then we process the appliction
        if ($inputDataArray['voucher'] == '') {
            #### now go and create the voucher via the api
            $createVoucherobj = CreateVoucher($voucherArray);
            #### create the voucher
            CreateVoucherProcess($createVoucherobj);
        } else {
            #### do a inquiry to check
            $makeInquiry = EnquiryRegardingVoucher($inputDataArray['voucher'], $inputDataArray['pin']);

            if ($makeInquiry['status'] == 'Error') {
                ### this is the error message
                $errorMsg = "Voucher Error - " . $makeInquiry['msg'];
                IntialiseVoucherPage($errorMsg);
            } else if ($makeInquiry['status'] == 'Valid') {
                $createVoucherobj = RechargeVoucher($voucherArray);
                ###t this is the voucher
                if ($createVoucherobj->result == '0') {
                    $successMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $successMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($successMsg, true);
                } else {
                    $ErrorMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $ErrorMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($ErrorMsg, true);
                }
                #### process the recharge.

            }

        }

    }

}

function redirectfromstripe()
{

    $inputDataArray = (isset($_SESSION['inputDataArray'])) ? $_SESSION['inputDataArray'] : '';
    $config         = handleConfig();

    $voucherArray = array(
        'serialnumber' => $inputDataArray['voucher'],
        'site'         => $config->site,
        'pin'          => $inputDataArray['pin'],
        'amount'       => $inputDataArray['amount'],
        'customername' => $inputDataArray['name'],
        'address'      => $inputDataArray['address'],
        'phone'        => $inputDataArray['mobile'],
        'email'        => $inputDataArray['email'],
        'isgift'       => $inputDataArray['gift'],
        'giftemail'    => $inputDataArray['gift'],
        'giftaddress'  => $inputDataArray['giftaddress'],
        'reference'    => '',
        'message'      => $inputDataArray['message'],
    );

    if($_REQUEST['hashcrsf'])
    {
    	#### this is the array 
    	 #### then we process the appliction
        if ($inputDataArray['voucher'] == '') {
            #### now go and create the voucher via the api
            $createVoucherobj = CreateVoucher($voucherArray);
            #### create the voucher
            CreateVoucherProcess($createVoucherobj);
        } else {
            #### do a inquiry to check
            $makeInquiry = EnquiryRegardingVoucher($inputDataArray['voucher'], $inputDataArray['pin']);

            if ($makeInquiry['status'] == 'Error') {
                ### this is the error message
                $errorMsg = "Voucher Error - " . $makeInquiry['msg'];
                IntialiseVoucherPage($errorMsg);
            } else if ($makeInquiry['status'] == 'Valid') {
                $createVoucherobj = RechargeVoucher($voucherArray);
                ###t this is the voucher
                if ($createVoucherobj->result == '0') {
                    $successMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $successMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($successMsg, true);
                } else {
                    $ErrorMsg = "<p>Voucher Number: " . $createVoucherobj->serialnumber . "</p>";
                    $ErrorMsg .= "<p>Voucher Recharged - " . $createVoucherobj->information . "</p>";
                    IntialiseVoucherPage($ErrorMsg, true);
                }
                #### process the recharge.

            }

    }

	}
}
