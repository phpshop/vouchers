jQuery(document).ready(function($) {

	$("#purchaseform").validate({
		submitHandler: function(form) {
				$(form).submit();
		}
	});

    /// this is the voucher system
    $("#enquiryform").validate({
        submitHandler: function(form) {
                $(form).submit();
        }
    });

	 $('input:checkbox[id="giftcheck"]').change(
        function(){
            if ($(this).is(':checked')) {
                $('#secondaryEmailArea').removeClass('d-none');
            }
            else
            {
            	$('#secondaryEmailArea').addClass('d-none');
            }
    });
    

  $('#emailValue').blur(function() {
        $('#pin').val($(this).val());
    });


});